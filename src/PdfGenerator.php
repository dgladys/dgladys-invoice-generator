<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 22:32
 */

namespace dgladys\invoicegenerator;


/**
 * Class PdfGenerator
 * @package dgladys\invoicegenerator\invoicegenerator
 */
class PdfGenerator
{
    /** @var  string Html which will be used to generate pdf */
    private $html;

    /** @var  string Charset of generated document */
    private $charset;

    /**
     * Initialize pdf generator with html
     * @param string $html
     * @param string $charset
     */
    public function __construct($html, $charset)
    {
        $this->html = $html;
        $this->charset = $charset;
    }

    /**
     * Generate pdf document
     * @return string
     */
    public function generate()
    {
        $generator = new \mPDF($this->charset ,'A4','','',20,15,5,25,10,10);
        $generator->SetProtection(array('print'));
        $generator->SetTitle("Invoice ".date('Y-m-d'));
        $generator->SetAuthor("H&H Ned-Pol");
        $generator->SetDisplayMode('fullpage');
        $generator->WriteHTML($this->html);
        return $generator->Output('', 'S');
    }
}