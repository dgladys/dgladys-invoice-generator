<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 19:40
 */

namespace dgladys\invoicegenerator;

use BadMethodCallException;

/**
 * Class Template
 * @package dgladys\invoicegenerator
 */
class Template
{
    /** @var  string Language of generated invoices */
    private $language;

    /** @var  string Default currency used in generated templates */
    private $currency;

    /** @var  string Template of invoice */
    private $template;

    /** @var  string Charset */
    private $charset;

    /** @var  LoggerInterface Logger */
    private $logger;

    /** @var  string Template dir */
    private $templateDir;

    /** @var  array */
    private $body = [];

    /** @var null | array Translations */
    private $translations = null;

    /**
     * Prepare template
     * @param array $configuration
     */
    public function __construct($configuration = [])
    {
        $allowedAttributes = ['language', 'currency', 'template', 'charset'];
        foreach ($allowedAttributes as $attribute) {
            $this->$attribute = $configuration['meta'][$attribute];
        }
        $this->body = $configuration['body'];
    }

    /**
     * Getter for currency
     * @return string
     */
    public function getCurrency()
    {
       return $this->currency;
    }

    /**
     * Getter for language
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Getter for template
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Getter for charset
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * Setter for `logger`
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * Getter for templateDir
     * @return string
     */
    private function getTemplateDir()
    {
        return $this->templateDir;
    }

    /**
     * Generate a html template
     * @return string
     * @throws BadMethodCallException
     */
    public function generate()
    {
        $this->templateDir = basename(dirname(__FILE__)).'/template/'.$this->getTemplate();
        if (file_exists($this->templateDir)) {

            if (file_exists($this->templateDir.'/index.php')) {
                if (file_exists($this->templateDir.'/lang/'.$this->getLanguage().'.php')) {
                    $this->translations = include($this->templateDir.'/lang/'.$this->getLanguage().'.php');
                }
                extract($this->body);
                ob_start();
                include($this->templateDir.'/index.php');
                return ob_get_clean();
            } else {
                throw new BadMethodCallException("Template ".$this->getTemplate().' has not index file');
            }

        } else {
            throw new BadMethodCallException('Template '.$this->getTemplate().' not exists');
        }
    }

    /**
     * Translate message if translation available
     * @param string $message
     * @return string
     */
    public function t($message)
    {
        if ($this->translations !== null && $this->getLanguage() != 'en' && isset($this->translations[$message])) {
            return $this->translations[$message];
        } else {
            return $message;
        }
    }
} 