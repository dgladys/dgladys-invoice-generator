<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 22:12
 */



/** @var \dgladys\invoicegenerator\Template $this */
/** @var array $company */
/** @var array $contractor */
/** @var array $invoice */
/** @var array $summary */
/** @var array $tax_summary */
/** @var array $positions */

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= $this->getCharset()  ?>"/>
        <style type="text/css">
            html, body {
                font-family: Arial, sans-serif;
            }

            div.invoice_header {
                position: relative;
                width: 100%;
                height: 50px;
            }

            div.invoice_header div.logo {
                position: relative;
                float: left;
                width: 350px;
            }

            div.invoice_header div.logo h1 {
                padding-top: 20px;
                padding-left: 15px;
                font-size: 36px;
            }

            div.invoice_header div.header_right {
                position: relative;
                width: 250px;
                float: right;
                font-size: 10px;
                text-align: right;
            }

            div.section_companies {
                position: relative;
                width: 100%;
            }

            div.section_companies div.company, div.section_companies div.contractor {
                position: relative;
                width: 48%;
                float: left;
            }

            div.section_companies div.contractor {
                float: right;
            }

            div.section_companies div.company h3, div.section_companies div.contractor h3 {
                padding-bottom: 0;
                margin-bottom: 0;
            }

            div.section_companies div.company div.line-horizontal,
            div.section_companies div.contractor div.line-horizontal {
                position: relative;
                width: 100%;
                height: 1.5px;
                background: #000000;
            }

            div.section_companies div.company p.description,
            div.section_companies div.contractor p.description {
                font-size: 10px;
                padding-top: 2px;
                margin-top: 2px;
            }

            div.section_companies div.contractor {
                width: 48%;
            }

            table.products_table {
                position: relative;
                width: 100%;
                margin-top: 30px;
                border-spacing:0; /* Removes the cell spacing via CSS */
                border-collapse: collapse;
            }

            table.products_table th {
            }

            table.products_table thead th, table.vat_sum thead th {
                background: #AAA;
                font-size: 12px;
                font-weight: normal;
                padding: 6px 5px;
            }

            table.products_table tbody td {
                background: #EEE;
                padding: 6px 2px;
                font-size: 12px;
            }

            table.products_table td, table.products_table th, table.vat_sum td, table.vat_sum th {
                border:0.1mm solid black;
            }

            table.products_table tbody td:nth-child(1), table.products_table tbody td:nth-child(2) {
                padding-left: 5px;
            }

            table.products_table tbody td:nth-child(3), table.products_table tbody td:nth-child(4),
            table.products_table tbody td:nth-child(5), table.products_table tbody td:nth-child(6),
            table.products_table tbody td:nth-child(7), table.products_table tbody td:nth-child(8){
                text-align: right;
                padding-right: 5px;
            }

            table.vat_sum {
                position: relative;
                margin-top: 10px;
                width: 100%;
                margin-left: 50%;
                border-spacing:0; /* Removes the cell spacing via CSS */
                border-collapse: collapse;
            }

            table.vat_sum td, table.vat_sum th {
                font-size : 12px;
                padding: 5px 4px;
            }

            div.invoice_summary {
                position: relative;
                margin-top: 35px;
                width: 100%;
                margin-left: 50%;
                font-size: 12px;
            }

            div.to-pay {
                position: relative;
                margin-left: 50%;
                width: 100%;
            }

            div.signatures {
                position: relative;
                width: 100%;
                margin-top: 40px;
            }

            div.signature {
                position: relative;
                width: 50%;
                float: left;
            }

            div.signatures div.signature:nth-child(2) div. {
                float: right;
            }



            div.signatures div.signature div.signature-line {
                position: relative;
                width: 80%;
                border-bottom : 0.5mm dashed black;
                margin-bottom: 5px;
            }

            div.signatures div.signature span {
                font-size: 12px;

            }

            div.signatures div.signature:nth-child(2) div.signature-line {
                float: right;
            }

        </style>
    </head>
    <body>
        <div class="invoice_header">
            <div class="logo">
                <h1><?= $company['name'] ?></h1>
            </div>
            <div class="header_right">
                <span><?= $this->t('Invoice place:').' <strong>'.$company['city'].'</strong>' ?></span> <br />
                <span><?= $this->t('Invoice date:').' <strong>'.$invoice['invoice_date'].'</strong>' ?></span> <br />
            </div>
        </div>
        <h2><?= $this->t('Invoice nr:').' '.$invoice['number'] ?></h2>
        <div class="section_companies">
            <div class="company">
                <h3><?= $this->t('Seller') ?></h3>
                <div class="line-horizontal"></div>
                <p class="description">
                    <strong><?= $company['name'] ?></strong> <br />
                    <strong><?= $company['address'] ?></strong> <br />
                    <strong><?= $company['postal_code'].' '.$company['city'] ?></strong> <br />
                    <span>NIP: </span> <strong><?= $company['nip'] ?></strong> <br />
                    <span><?= $this->t('Bank account:') ?> </span> <strong><?= $company['bank_account'] ?></strong> <br />
                </p>
            </div>
            <div class="contractor">
                <h3><?= $this->t('Buyer') ?></h3>
                <div class="line-horizontal"></div>
                <p class="description">
                    <strong><?= $contractor['name'] ?></strong> <br />
                    <strong><?= $contractor['address'] ?></strong> <br />
                    <strong><?= $contractor['postal_code'].' '.$contractor['city'] ?></strong> <br />
                    <span>NIP: </span> <strong><?= $contractor['nip'] ?></strong> <br />
                </p>
            </div>
        </div>

        <table class="products_table">
            <thead>
                <tr>
                <th></th>
                <th><?= $this->t('Product name') ?></th>
                <th><?= $this->t('Quantity') ?></th>
                <th><?= $this->t('Netto price') ?></th>
                <th><?= $this->t('Vat') ?></th>
                <th><?= $this->t('Netto sum') ?></th>
                <th><?= $this->t('Vat sum') ?></th>
                <th><?= $this->t('Brutto sum') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($positions as $position): ?>
                <tr>
                    <td><?= $position['lp'] ?></td>
                    <td><?= $position['name'] ?></td>
                    <td><?= $position['quantity'] ?></td>
                    <td><?= $position['netto_price'] ?></td>
                    <td><?= $position['tax_rate'] ?></td>
                    <td><?= $position['netto'] ?></td>
                    <td><?= $position['tax'] ?></td>
                    <td><?= $position['brutto'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <table class="vat_sum">
            <thead>
            <tr>
                <th><?= $this->t('VAT tariff') ?></th>
                <th><?= $this->t('Netto') ?></th>
                <th><?= $this->t('Vat') ?></th>
                <th><?= $this->t('Brutto') ?></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($tax_summary as $record): ?>
                <tr>
                    <td><?= preg_match('/^[0-9]+%$/', $record['tax_rate']) ? $record['tax_rate'] : $this->t($record['tax_rate']) ?></td>
                    <td><?= $record['netto'] ?></td>
                    <td><?= $record['tax'] ?></td>
                    <td><?= $record['brutto'] ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <div class="invoice_summary">
            <span><?= $this->t('Payment method:') ?> <strong><?=$this->t($summary['payment_method'])  ?></strong></span> <br />
            <span><?= $this->t('Due date:') ?><strong><?= $invoice['due_date'].'('.$invoice['due_days'].' '.$this->t('days').')' ?></strong></span>
        </div>

        <div class="to-pay">
            <h2><?= $this->t('To pay:').' '.(count($tax_summary) ? $tax_summary[count($tax_summary)-1]['brutto'] : '0.00').' '.$this->getCurrency()  ?></h2>
        </div>

        <div class="signatures">
            <div class="signature">
                <div class="signature-line"></div>
                <span><?= $this->t('seller signature') ?></span>
            </div>
            <div class="signature">
                <div class="signature-line"></div>
                <span><?= $this->t('buyer signature') ?></span>
            </div>
        </div>

    </body>
</html>