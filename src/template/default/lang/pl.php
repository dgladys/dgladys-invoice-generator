<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 22:22
 */

return [
    'Invoice nr:' => 'Faktura nr:',
    'Invoice place:' => 'Miejsce wystawienia:',
    'Invoice date:' => 'Data wystawienia:',
    'Seller' => 'Wystawca',
    'Buyer' => 'Odbiorca',
    'Bank account:' => 'Numer konta bankowego:',
    'Total' => 'Razem',
    'Product name' => 'Nazwa produkty',
    'Quantity' => 'Ilość',
    'Netto price' => 'Cena netto',
    'Vat' => 'VAT',
    'VAT' => 'VAT',
    'Netto sum' => 'Suma Netto',
    'Brutto sum' => 'Suma Brutto',
    'Vat sum' => 'Suma VAT',
    'summary' => 'Razem',
    'days' => 'dni',
    'Payment method:' => 'Metoda płatności: ',
    'Due date:' => 'Termin płatności: ',
    'To pay:' => 'Do zapłaty:',
    'seller signature' => 'Podpis wystawcy',
    'buyer signature' => 'Podpis odbiorcy',
	'bank_transfer' => 'przelew bankowy'
];