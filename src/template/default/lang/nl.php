<?php
/**
 * Created by PhpStorm.
 * User: Damian Gładys <damian.gladys.cracow@gmail.com>
 * Date: 2015-08-10
 * Time: 22:14
 */

return [
    'Invoice nr:' => 'Factuur no:',
    'Invoice place:' => 'Plaats van afgifte:',
    'Invoice date:' => 'Datum van afgifte:',
    'Seller' => 'Verkoper',
    'Buyer' => 'Koper',
    'Bank account:' => 'Rekeningnummer:',
    'Total' => 'Samen',
    'Product name' => 'Naam van product dienst',
    'Quantity' => 'Hoeveelheid',
    'Netto price' => 'Netto prijs',
    'Vat' => 'VAT',
    'VAT' => 'VAT',
    'Netto sum' => 'Netto bedrag',
    'Brutto sum' => 'Brutto bedrag',
    'Vat sum' => 'VAT bedrag',
    'summary' => 'Samen',
    'days' => 'dagen',
    'Payment method:' => 'Betaalmethode: ',
    'Due date:' => 'Deadline van die betaling: ',
    'To pay:' => 'Ta betalen:',
    'seller signature' => 'Bevoegde personen om te ontvangen',
    'buyer signature' => 'Bevoegde personen om te ontvangen',
	'bank_transfer' => 'overschrijving'
];