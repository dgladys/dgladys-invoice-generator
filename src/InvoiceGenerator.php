<?php

namespace dgladys\invoicegenerator;

use BadMethodCallException;
use InvalidArgumentException;

/**
* Class InvoiceGenerator
* @package dgladys\invoicegenerator
*/
class InvoiceGenerator
{
    /** @var  string Language of generated invoices */
    private $language;

    /** @var  string Charset used in generator. Compatible with mPDF. */
    private $charset;

    /** @var  string Default currency used in generated templates */
    private $currency;

    /** @var  LoggerInterface Logger class */
    private $logger;

    /** @var  string Template of invoice */
    private $template;

    /**
     * Get list of supported languages
     * @return array
     */
    private function supportedLanguages()
    {
        return ['pl', 'nl', 'en'];
    }

    /**
     * Configure component
     * @param array $config
     * @throws BadMethodCallException
     */
    public function __construct($config = [])
    {
        //Language property
        $language = isset($config['language']) ? $config['language'] : 'pl';
        if (!in_array($language, $this->supportedLanguages())) {
            throw new BadMethodCallException('Unsupported language: '.$language);
        }
        $this->language = $language;

        //Charset property
        $charset = isset($config['charset']) ? $config['charset'] : 'utf-8';
        $this->charset = $charset;

        //Currency property
        $currency = isset($config['currency']) ? $config['currency'] : 'EUR';
        $this->currency = $currency;

        //Logger init
        $logger = (isset($config['logger']) && class_exists($config['logger']))
            ? $config['logger']
            : 'dgladys\invoicegenerator\DefaultLogger';
        $this->logger = new $logger;

        //Template property
        $this->template = 'default';

        $loggerInfo = [
            'language' => $this->language,
            'charset' => $this->charset,
            'currency' => $this->currency,
            'logger' => get_class($this->logger),
            'template' => $this->template
        ];

        $this->logger->info(
            "__construct(): Initialize object with data: ".json_encode($loggerInfo),
            'dgladys\invoicegenerator\InvoiceGenerator'
        );
    }

    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Generate content of invoice pdf file.
     * Use template chosen by setTemplate call
     * @param string | array $invoiceData
     * @throws InvalidArgumentException
     * @return string
     */
    public function generate($invoiceData)
    {
        $data = is_string($invoiceData) ? json_decode($invoiceData, 1) : $invoiceData;
        if (!isset($data['body']) || !isset($data['meta'])) {
            throw new InvalidArgumentException('Invalid template: '.json_encode($data));
        }
        $meta = [
            'template' => $this->template,
            'currency' => $this->currency,
            'language' => $this->language,
            'charset'  => $this->charset
        ];

        foreach ($meta as $key => $value) {
            if (!isset($data['meta'][$key])) {
                $data['meta'][$key] = $value;
            }
        }

        $template = new Template($data);
        $template->setLogger($this->logger);
        $html = $template->generate();

        $pdfGenerator = new PdfGenerator($html, $this->charset);
        return $pdfGenerator->generate();

    }
	
}