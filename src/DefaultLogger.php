<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 19:18
 */

namespace dgladys\invoicegenerator;

/**
 * Class DefaultLogger
 * Default logger which do nothing
 * @package dgladys\invoicegenerator
 */
class DefaultLogger implements LoggerInterface
{
    /**
     * @param string $message
     * @param string $category
     */
    public function error($message, $category)
    {
        return;
    }

    /**
     * @param string $message
     * @param string $category
     */
    public function warning($message, $category)
    {
        return;
    }

    /**
     * @param string $message
     * @param string $category
     */
    public function info($message, $category)
    {
        return;
    }

    /**
     * @param string $message
     * @param string $category
     */
    public function trace($message, $category)
    {
        return;
    }
}