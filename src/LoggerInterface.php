<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 10.08.15
 * Time: 19:15
 */

namespace dgladys\invoicegenerator;

/**
 * Interface LoggerInterface
 * Base interface for external logger.
 * Use with pattern Adapter.
 * @package dgladys\invoicegenerator
 */
interface LoggerInterface {
    public function error($message, $category);

    public function warning($message, $category);

    public function info($message, $category);

    public function trace($message, $category);
} 