<?php 

include('vendor/autoload.php');



use dgladys\invoicegenerator\InvoiceGenerator;

$json = '{
    "body": {
        "contractor": {
            "name": "adasd",
            "nip": "123456788",
            "city": "asd",
            "address": "asda",
            "postal_code": "asdasd",
            "bank_account": "324234"
        },
        "company": {
            "name": "test",
            "nip": "1234567890",
            "city": "Amsterdam",
            "address": "Ul. Sezamkowa 13",
            "postal_code": "31-414",
            "bank_account": "1700 4533 6754 3453 3453 4353"
        },
        "invoice": {
            "invoice_date": "2015-08-07",
            "service_date": "2015-08-07",
            "due_date": "2015-08-07",
            "due_days": "0",
            "number": "2015\/08\/07\/123"
        },
        "positions": [
            {
                "lp": 1,
                "name": "Produkt",
                "quantity": "1.00",
                "unit": "piece",
                "netto_price": "3.45",
                "tax_rate": "6%",
                "netto": 3.45,
                "tax": 0.21,
                "brutto": 3.66
            }
        ],
        "tax_summary": [
            {
                "tax_rate": "6%",
                "netto": 3.45,
                "tax": 0.21,
                "brutto": 3.66
            },
            {
                "tax_rate": "summary",
                "netto": 3.45,
                "tax": 0.21,
                "brutto": 3.66
            }
        ],
        "summary": {
            "payment_method": "bank_transfer",
            "shipment_method": "personal"
        }
    },
    "meta": {
        "language": "nl",
        "charset": "utf8",
        "currency": "EUR"
    }
}';

$invoiceGenerator = new InvoiceGenerator([
	'language' => 'pl',
	'charset' => 'utf-8',
	'currency' => 'EUR'
]);
$invoiceGenerator->setTemplate('default');
echo $invoiceGenerator->generate($json);